import React from "react";
import { Provider } from "react-redux";
import { useFonts } from "expo-font";

import { store } from "./src/app/store";
import { NavigationContainer } from "@react-navigation/native";
import HomeTabNavigator from "./src/navigation/HomeTabNavigator";

export default function App() {
  const [fontLoaded] = useFonts({
    "open-sans": require("./assets/fonts/OpenSans-Regular.ttf"),
    "open-sans-bold": require("./assets/fonts/OpenSans-Bold.ttf"),
  });
  if (!fontLoaded) {
    return null;
  }
  return (
    <Provider store={store}>
      <NavigationContainer>
        <HomeTabNavigator />
      </NavigationContainer>
    </Provider>
  );
}
