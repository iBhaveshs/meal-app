import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import { Ionicons } from "@expo/vector-icons";

import FavouritesScreen from "../screens/FavouritesScreen";
import Colors from "../constants/Colors";
import MealsNavigator from "./MealsNavigator";

export type RootTabParamList = {
  Home: undefined;
  Favourites: undefined;
};

const Tab = createBottomTabNavigator<RootTabParamList>();

const HomeTabNavigator = () => {
  return (
    <Tab.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: Colors.primaryColor,
        },
        headerTintColor: "white",
        headerTitleStyle: {
          fontFamily: "open-sans",
        },
        tabBarActiveTintColor: Colors.accentColor,
        tabBarIcon: undefined,
      }}
    >
      <Tab.Screen
        name="Home"
        component={MealsNavigator}
        options={{
          headerShown: false,
          tabBarIcon: ({ focused, color }) => (
            <Ionicons
              color={color}
              name={focused ? "ios-restaurant" : "ios-restaurant-outline"}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Favourites"
        component={FavouritesScreen}
        options={{
          tabBarIcon: ({ focused, color }) => (
            <Ionicons
              color={color}
              name={focused ? "heart" : "heart-outline"}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default HomeTabNavigator;
