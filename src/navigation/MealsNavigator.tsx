import React from "react";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import { StatusBar } from "expo-status-bar";

import CategoryMealScreen from "../screens/CategoryMealScreen";
import MealDetailScreen from "../screens/MealDetailScreen";
import Colors from "../constants/Colors";
import Category from "../models/Category";
import Meal from "../models/Meal";
import CategoriesScreen from "../screens/CategoriesScreen";

export type RootStackParamList = {
  Categories: undefined;
  CategoryMeals: { category: Category };
  MealDetail: { meal: Meal };
};

const Stack = createNativeStackNavigator<RootStackParamList>();

const MealsNavigator = () => {
  return (
    <>
      <StatusBar style="inverted" />
      <Stack.Navigator
        screenOptions={{
          headerStyle: {
            backgroundColor: Colors.primaryColor,
          },
          headerTintColor: "white",
          headerTitleStyle: {
            fontFamily: "open-sans",
          },
        }}
      >
        <Stack.Screen name="Categories" component={CategoriesScreen} />
        <Stack.Screen
          name="CategoryMeals"
          component={CategoryMealScreen}
          options={({ route }) => ({ title: route.params.category.title })}
        />
        <Stack.Screen
          name="MealDetail"
          component={MealDetailScreen}
          options={({ route }) => ({
            title: route.params.meal.title,
          })}
        />
      </Stack.Navigator>
    </>
  );
};

export default MealsNavigator;
