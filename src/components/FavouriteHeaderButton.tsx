import React from "react";
import {
  HeaderButton,
  HeaderButtonProps,
} from "react-navigation-header-buttons";
import { Ionicons } from "@expo/vector-icons";

import Colors from "../constants/Colors";
import { Platform } from "react-native";

interface Props extends HeaderButtonProps {}

const FavouriteHeaderButton = (props: Props) => {
  return (
    <HeaderButton
      {...props}
      IconComponent={Ionicons}
      iconSize={20}
      color={Platform.OS === "android" ? "white" : Colors.primaryColor}
    />
  );
};

export default FavouriteHeaderButton;
