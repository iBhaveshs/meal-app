import React, { ComponentType } from "react";
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Platform,
  TouchableNativeFeedback,
  TouchableOpacityProps,
  TouchableNativeFeedbackProps,
} from "react-native";
import styled from "styled-components/native";

import Category from "../models/Category";

const Title = styled.Text`
  font-family: open-sans-bold;
  font-size: 20px;
  text-align: right;
`;

type Props = {
  item: Category;
  onPress: () => void;
};

const CategoryGridItem = (props: Props) => {
  let TouchableCmp: ComponentType<
    TouchableOpacityProps | TouchableNativeFeedbackProps
  > = TouchableOpacity;

  if (Platform.OS === "android" && Platform.Version >= 21) {
    TouchableCmp = TouchableNativeFeedback;
  }
  return (
    <View style={styles.gridItem}>
      <TouchableCmp style={styles.touchableStyle} onPress={props.onPress}>
        <View style={{ ...styles.gridView, backgroundColor: props.item.color }}>
          <Title numberOfLines={2}>{props.item.title}</Title>
        </View>
      </TouchableCmp>
    </View>
  );
};

const styles = StyleSheet.create({
  gridItem: {
    flex: 1,
    height: 150,
    margin: 15,
    borderRadius: 10,
    overflow: "hidden",
    elevation: 5,
  },
  gridView: {
    flex: 1,
    padding: 16,
    justifyContent: "flex-end",
    alignItems: "flex-end",
    borderRadius: 10,
    shadowColor: "black",
    shadowOpacity: 0.26,
    shadowOffset: { width: 0, height: 2 },
    shadowRadius: 10,
  },
  touchableStyle: {
    flex: 1,
  },
});

export default CategoryGridItem;
