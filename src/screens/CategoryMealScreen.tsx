import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React from "react";
import { FlatList, ListRenderItem, StyleSheet, View } from "react-native";
import MealItem from "../components/MealItem";
import { MEALS } from "../data/dummy-data";
import Meal from "../models/Meal";
import { RootStackParamList } from "../navigation/MealsNavigator";

type Props = NativeStackScreenProps<RootStackParamList, "CategoryMeals">;

const CategoryMealScreen = ({ navigation, route }: Props) => {
  const meals = MEALS.filter(
    (meal) => meal.categoryIds.indexOf(route.params.category.id) >= 0
  );
  const goToMealDetail = (meal: Meal) => {
    navigation.navigate("MealDetail", { meal });
  };

  const renderItem: ListRenderItem<Meal> = ({ item }) => (
    <MealItem
      meal={item}
      onSelectMeal={() => {
        goToMealDetail(item);
      }}
    />
  );

  return (
    <View style={styles.screen}>
      <FlatList style={styles.flatList} data={meals} renderItem={renderItem} />
    </View>
  );
};

const styles = StyleSheet.create({
  screen: {
    flex: 1,
    justifyContent: "center",
    padding: 16,
  },
  flatList: {
    width: "100%",
  },
});

export default CategoryMealScreen;
