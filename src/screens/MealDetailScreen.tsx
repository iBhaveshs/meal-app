import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React, { useLayoutEffect, useState } from "react";
import { View, Text, StyleSheet } from "react-native";
import { HeaderButtons, Item } from "react-navigation-header-buttons";
import FavouriteHeaderButton from "../components/FavouriteHeaderButton";
import { RootStackParamList } from "../navigation/MealsNavigator";

type Props = NativeStackScreenProps<RootStackParamList, "MealDetail">;

const MealDetailScreen = ({ navigation, route }: Props) => {
  const [isFavorite, setIsFavorite] = useState(false);

  const iconName = isFavorite ? "heart" : "heart-outline";

  const toggleFavourite = () => {
    setIsFavorite((value) => !value);
  };

  useLayoutEffect(() => {
    navigation.setOptions({
      headerRight: () => (
        <HeaderButtons HeaderButtonComponent={FavouriteHeaderButton}>
          <Item
            title="Fav"
            iconName={iconName}
            onPress={toggleFavourite}
            // eslint-disable-next-line react-native/no-inline-styles
            style={{ marginRight: -15 }}
          />
        </HeaderButtons>
      ),
    });
  }, [navigation, iconName]);
  return (
    <View style={styles.container}>
      <Text>{route.params.meal.title}</Text>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    fontSize: 20,
  },
});

export default MealDetailScreen;
