import { NativeStackScreenProps } from "@react-navigation/native-stack";
import React from "react";
import { FlatList, ListRenderItem } from "react-native";
import { RootStackParamList } from "../navigation/MealsNavigator";

import { CATEGORIES } from "../data/dummy-data";
import Category from "../models/Category";
import CategoryGridItem from "../components/CategoryGridItem";

type Props = NativeStackScreenProps<RootStackParamList, "Categories">;

const CategoriesScreen = ({ navigation }: Props) => {
  const goToMeals = (category: Category) => {
    navigation.navigate("CategoryMeals", { category });
  };

  const renderGridItem: ListRenderItem<Category> = ({ item }) => (
    <CategoryGridItem
      item={item}
      onPress={() => {
        goToMeals(item);
      }}
    />
  );

  return (
    <FlatList data={CATEGORIES} renderItem={renderGridItem} numColumns={2} />
  );
};

export default CategoriesScreen;
